package sorting;


public class Sorter {

    /**
     * Some sorting algorithm that possibly the slowest algorithm.
     *
     * @param inputArr array of integer that need to be sorted.
     * @return a sorted array of integer.
     */

    public static int[] slowSort(int[] inputArr) {
        int temp;
        for (int i = 1; i < inputArr.length; i++) {
            for (int j = i; j > 0; j--) {
                if (inputArr[j] < inputArr[j - 1]) {
                    temp = inputArr[j];
                    inputArr[j] = inputArr[j - 1];
                    inputArr[j - 1] = temp;
                }
            }
        }
        return inputArr;
    }

    public static void quickSort(int[] inputArr) {
        quickSort(inputArr, 0, inputArr.length - 1);
    }

    private static void quickSort(int[] inputArr, int low, int high) {
        int i = low;
        int j = high;

        int pivot = inputArr[low + (high - low) / 2];
        while (i <= j) {

            while (inputArr[i] < pivot) {
                i++;

            }
            while (inputArr[j] > pivot) {
                j--;
            }
            if (i <= j) {
                int temp = inputArr[i];
                inputArr[i] = inputArr[j];
                inputArr[j] = temp;
                i++;
                j--;
            }
        }

        if (low < j) {

            quickSort(inputArr, low, j);
        }
        if (i < high) {

            quickSort(inputArr, i, high);
        }
    }
}
