package sorting;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import static sorting.Finder.binarySearch;
import static sorting.Finder.slowSearch;

import static sorting.Sorter.quickSort;
import static sorting.Sorter.slowSort;

import java.util.Arrays;

import org.junit.Test;

public class SortAndSearchTest {
    private static final int[] sortedArr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    @Test
    public void testSlowSort() {
        int[] arr = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
        slowSort(arr);
        assertTrue(Arrays.equals(arr, sortedArr));
    }

    @Test
    public void testQuickSort() {
        int[] arr = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
        quickSort(arr);
        assertTrue(Arrays.equals(arr, sortedArr));
    }

    @Test
    public void testSlowSearch() {
        int[] arr = {1,9,2,8,3,7,4,5,6};
        assertEquals(slowSearch(arr, 6), 6);
        assertEquals(slowSearch(arr, 100), -1);
    }

    @Test
    public void testBinarySearch() {
        assertEquals(binarySearch(sortedArr, 6), 6);
        assertEquals(binarySearch(sortedArr, 100), -1);
    }
}
