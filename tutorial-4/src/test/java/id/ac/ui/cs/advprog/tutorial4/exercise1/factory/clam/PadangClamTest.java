package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class PadangClamTest {
    private PadangClams padangClams;

    @Before
    public void setUp() {
        padangClams = new PadangClams();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Padang Clams from Padang City", padangClams.toString());
    }
}

