package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.junit.Assert.assertNotNull;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import org.junit.Before;
import org.junit.Test;

public class CheesePizzaTest {
    private CheesePizza cheesePizza;
    private DepokPizzaIngredientFactory depokPizzaIngredientFactory;

    @Before
    public void setUp() {
        depokPizzaIngredientFactory = new DepokPizzaIngredientFactory();
        cheesePizza = new CheesePizza(depokPizzaIngredientFactory);
    }

    @Test
    public void testMethodPrepare() {
        cheesePizza.prepare();
        assertNotNull(cheesePizza.dough);
        assertNotNull(cheesePizza.sauce);
        assertNotNull(cheesePizza.cheese);
    }
}
