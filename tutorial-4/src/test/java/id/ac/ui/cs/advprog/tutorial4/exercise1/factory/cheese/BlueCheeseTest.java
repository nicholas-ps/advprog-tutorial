package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;


public class BlueCheeseTest {
    private BlueCheese blueCheese;

    @Before
    public void setUp() {
        blueCheese = new BlueCheese();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Blue Cheese", blueCheese.toString());
    }

}
