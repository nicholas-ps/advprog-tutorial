package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CornTest {
    private Corn corn;

    @Before
    public void setUp() {
        corn = new Corn();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Corn", corn.toString());
    }
}
