package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CabaiSauceTest {
    private CabaiSauce cabaiSauce;

    @Before
    public void setUp() {
        cabaiSauce = new CabaiSauce();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Cabai Sauce extra hot", cabaiSauce.toString());
    }
}