package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.junit.Assert.assertNotNull;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import org.junit.Before;
import org.junit.Test;

public class VeggiePizzaTest {
    private VeggiePizza veggiePizza;
    private DepokPizzaIngredientFactory depokPizzaIngredientFactory;

    @Before
    public void setUp() {
        depokPizzaIngredientFactory = new DepokPizzaIngredientFactory();
        veggiePizza = new VeggiePizza(depokPizzaIngredientFactory);
    }

    @Test
    public void testMethodPrepare() {
        veggiePizza.prepare();
        assertNotNull(veggiePizza.dough);
        assertNotNull(veggiePizza.sauce);
        assertNotNull(veggiePizza.veggies);
    }
}
