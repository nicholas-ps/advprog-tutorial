package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class CabaiSauce implements Sauce {
    public String toString() {
        return "Cabai Sauce extra hot";
    }
}
