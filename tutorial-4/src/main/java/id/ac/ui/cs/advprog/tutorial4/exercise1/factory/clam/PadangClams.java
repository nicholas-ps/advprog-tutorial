package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class PadangClams implements Clams {
    public String toString() {
        return "Padang Clams from Padang City";
    }
}
