package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.CrustySandwich;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.BeefMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Cheese;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.ChiliSauce;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Lettuce;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.TomatoSauce;

public class MainDecorator {
    public static void main(String[] args) {
        Food food = new CrustySandwich();
        System.out.printf("makanan ini terdiri dari %s seharga %f%n%n", 
            food.getDescription(), food.cost());
        food = new BeefMeat(food);
        System.out.printf("makanan ini terdiri dari %s seharga %f%n%n", 
            food.getDescription(), food.cost());
        food = new Cheese(food);
        System.out.printf("makanan ini terdiri dari %s seharga %f%n%n", 
            food.getDescription(), food.cost());
        food = new ChiliSauce(food);
        System.out.printf("makanan ini terdiri dari %s seharga %f%n%n", 
            food.getDescription(), food.cost());
        food = new Lettuce(food);
        System.out.printf("makanan ini terdiri dari %s seharga %f%n%n", 
            food.getDescription(), food.cost());
        food = new TomatoSauce(food);
        System.out.printf("makanan ini terdiri dari %s seharga %f%n%n", 
            food.getDescription(), food.cost());
    }
}
