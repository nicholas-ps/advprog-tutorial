package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

public class MainComposite {
    public static void main(String[] args) {
        Company company = new Company();
        Ceo niko = new Ceo("niko", 500000.00);
        Cto reza = new Cto("reza", 320000.00);;
        BackendProgrammer william = new BackendProgrammer("william", 94000.00);
        FrontendProgrammer kevin = new FrontendProgrammer("kevin",66000.00);
        UiUxDesigner donny = new UiUxDesigner("donny", 177000.00);
        NetworkExpert dio = new NetworkExpert("dio", 83000.00);
        SecurityExpert edwin = new SecurityExpert("edwin", 70000.00);
        company.addEmployee(niko);
        company.addEmployee(reza);
        company.addEmployee(william);
        company.addEmployee(kevin);
        company.addEmployee(donny);
        company.addEmployee(dio);
        company.addEmployee(edwin);
        for (Employees employee : company.getAllEmployees()) {
            System.out.printf("Employee %s sebagai %s dengan salary %f%n", employee.getName(), 
                employee.getRole(), employee.getSalary());
        }
        System.out.printf("Total salary adalah %f%n", company.getNetSalaries());
    }
}
