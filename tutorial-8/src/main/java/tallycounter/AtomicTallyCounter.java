package tallycounter;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicTallyCounter {
    private AtomicInteger counter = new AtomicInteger(0);

    public void increment() {
        this.counter.getAndIncrement();
    }

    public void decrement() {
        this.counter.getAndDecrement();
    }

    public int value() {
        return counter.get();
    }
}
